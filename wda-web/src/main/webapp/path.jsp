<%@page import="org.apache.commons.fileupload.FileItem"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="org.apache.commons.fileupload.servlet.ServletFileUpload"%>
<%@page import="org.apache.commons.fileupload.disk.DiskFileItemFactory"%>
<%@page import="java.net.URLDecoder"%>
<%@page import="com.farm.wda.util.HtmlUtils"%>
<%@page import="com.farm.wda.util.Md5Utils"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="com.farm.wda.inter.WdaAppInter"%>
<%@page import="com.farm.wda.util.AppConfig"%>
<%@page import="java.io.File"%>
<%@page import="com.farm.wda.Beanfactory"%>
<%@ page language="java" pageEncoding="utf-8"%>
<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><%=AppConfig.getString("config.web.title")%></title>
<link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<%
	boolean isOnlyOne = false;
	String curentUrl = null;
	String paht = request.getParameter("path");
	String key = request.getParameter("key");
	String url = request.getParameter("url");
	String typename = request.getParameter("typename");
	String filename = request.getParameter("filename");
%>
<%
	//验证上传的内容类型
	String contentType = request.getContentType();
	if (contentType != null && (contentType.indexOf("multipart/form-data") >= 0)) {
		DiskFileItemFactory factory = new DiskFileItemFactory();
		//设置内存中存储文件的最大值
		factory.setSizeThreshold(100 * 1024 * 1024);
		//本地存储的数据大于maxMemSize
		factory.setRepository(new File(Beanfactory.WEB_DIR + File.separator + "temp"));
		//床架一个新的文件上传处理程序
		ServletFileUpload upload = new ServletFileUpload(factory);
		//设置最大上传的文件大小
		upload.setSizeMax(100 * 1024 * 1024);
		try {
			//解析获取的文件
			List<FileItem> fileItems = upload.parseRequest(request);
			//处理上传的文件
			Iterator<FileItem> i = fileItems.iterator();
			while (i.hasNext()) {
				FileItem fi = (FileItem) i.next();
				if (!fi.isFormField()) {
					//获取上传文件的参数
					String fieldName = fi.getFieldName();
					System.out.print(fieldName);
					filename = fi.getName();
					boolean isInMemory = fi.isInMemory();
					long sizeInBytes = fi.getSize();
					//写入文件
					File file = new File(Beanfactory.WEB_DIR + File.separator
							+ Beanfactory.getFileKeyCoderImpl().parseDir(key) + File.separator + filename);
					file.getParentFile().mkdirs();
					fi.write(file);
					paht = file.getPath();
				}else{
					if(fi.getFieldName().equals("key")){
						key=fi.getString();
					}
					if(fi.getFieldName().equals("typename")){
						typename=fi.getString();
					}
				}
				
			} //hasNext

		} catch (Exception ex) {
			System.out.println(ex);
		}

	}
%>

<%
	
	String txt = null;
	if ("NONE".equals(typename)) {
		typename = null;
	}
	if (key == null || key.isEmpty()) {
		key = "none";
	}
	if (filename == null || filename.isEmpty()) {
		filename = key;
	}
	if(key!=null&&(key.trim().indexOf("http")==0||key.trim().indexOf("https")==0)){
		key=URLDecoder.decode(key,"utf8");
		key=URLDecoder.decode(key,"utf8");
		key= Md5Utils.MD5(key);
	}
	WdaAppInter wad = Beanfactory.getWdaAppImpl();
	if(StringUtils.isNotBlank(url)){
		//由url创建
		key = Md5Utils.MD5(url);
		filename=url.substring(url.lastIndexOf("/")+1);
		filename=URLDecoder.decode(filename,"utf8");
		if(filename.indexOf("?")>0){
			filename=filename.substring(0, filename.indexOf("?"));
		}
		filename=filename.trim().replaceAll(" ", "");
		if(!wad.isLoged(key)){
			File file=	HtmlUtils.downloadWebFile(key,url,filename);
			paht=file.getPath();
		}
	}
	boolean isError=wad.getLogText(key)==null?false:wad.getLogText(key).indexOf("error")>0;
	if (AppConfig.getString("config.web.submit").equals("true")) {
		if (paht != null && key != null && typename != null && !paht.isEmpty() && !key.isEmpty()
				&& !typename.isEmpty()) {
			wad.generateDoc(key, new File(paht), typename, filename, "none");
		} else {
			if (paht != null && key != null && !paht.isEmpty() && !key.isEmpty()) {
				wad.generateDoc(key, new File(paht), filename, "none");
			}
		}
	}
%>
<body style="background-color: #8a8a8a;">
	<jsp:include page="/commons/head.jsp"></jsp:include>
	<div class="container">
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-body text-center">
						<%=wad.getInfo(key)%>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">key:<%=key %></div>
					<div class="panel-body">
						<table class="table">
						<tr>
								<td style="text-align: center;">
									<!-- ----各种文件预览图标生成--开始--------------------------------------------------------------------- -->
									<%
										boolean isHaveFile = false;
										if (wad.isGenerated(key, "TXT")) {
											isHaveFile = true;
											if (curentUrl != null) {
												isOnlyOne = false;
											} else {
												curentUrl = wad.getUrl(key, "TXT");
												isOnlyOne = true;
											}
									%> <a href="<%=wad.getUrl(key, "TXT")%>"><img alt="TXT"
										src="img/txt.png"></a> <%
									 	}
									 %> <%
									 	if (wad.isGenerated(key, "HTML")) {
									 		isHaveFile = true;
									 		if (curentUrl != null) {
									 			isOnlyOne = false;
									 		} else {
									 			curentUrl = wad.getUrl(key, "HTML");
									 			isOnlyOne = true;
									 		}
									 %> <a href="<%=wad.getUrl(key, "HTML")%>"><img alt="HTML"
										src="img/html.png"></a> <%
									 	}
									 %> 
									 <%
									 	if (wad.isGenerated(key, "OVIDEO")) {
									 		isHaveFile = true;
									 		if (curentUrl != null) {
									 			isOnlyOne = false;
									 		} else {
									 			curentUrl = "showpage/video.jsp?key=" + key;
									 			isOnlyOne = true;
									 		}
									 %> <a href="showpage/video.jsp?key=<%=key%>"><img alt="HTML"
										src="img/video.png"></a>
									 <%}%> 
									 
									 
									 <%
									 	if (wad.isGenerated(key, "OAUDIO")) {
									 		isHaveFile = true;
									 		if (curentUrl != null) {
									 			isOnlyOne = false;
									 		} else {
									 			curentUrl = "showpage/audio.jsp?key=" + key;
									 			isOnlyOne = true;
									 		}
									 %> <a href="showpage/audio.jsp?key=<%=key%>"><img alt="HTML"
										src="img/audio.png"></a>
									 <%}%> 
									 
									 
									 <%
									 	if (wad.isGenerated(key, "FLV")) {
									 		isHaveFile = true;
									 		if (curentUrl != null) {
									 			isOnlyOne = false;
									 		} else {
									 			curentUrl = "flv.jsp?key=" + key;
									 			isOnlyOne = true;
									 		}
									 %> <a href="flv.jsp?key=<%=key%>"><img alt="HTML"
										src="img/flv.png"></a>
									 <%}%> 
									 
									 <%
									 	if (wad.isGenerated(key, "IMG")) {
									 		isHaveFile = true;
									 		if (curentUrl != null) {
									 			isOnlyOne = false;
									 		} else {
									 			curentUrl = "showpage/img.jsp?key=" + key;
									 			isOnlyOne = true;
									 		}
									 %> <a href="showpage/img.jsp?key=<%=key%>"><img alt="HTML"
										src="img/img.png"></a>
									 <%}%> 
									 
									 <%
									 	if (wad.isGenerated(key, "PDF")) {
									 		isHaveFile = true;
									 		if (curentUrl != null) {
									 			isOnlyOne = false;
									 		} else {
									 			curentUrl = wad.getUrl(key, "PDF");
									 			isOnlyOne = true;
									 		}
									 %> <a href="<%=wad.getUrl(key, "PDF")%>"><img alt="PDF"
										src="img/pdf.png"></a> <%
									 	}
									 %> 
									 <%
									 	if (isHaveFile && wad.isLoged(key)) {
									 %>
									<div style="text-align: center; color: #f15a24;">点击图标预览文件</div>
									<%
										}
									%> <%
									 	if (!isHaveFile && wad.isLoged(key)) {
									 %>
									<div style="text-align:center; color: #f15a24;">
										<img alt="Brand" src="img/type.png">
									</div> <%
										}
									%><!-- ----各种文件预览图标生成--结束--------------------------------------------------------------------- -->
									<%if(!isError){%>	
										<%
										 	if (!isHaveFile && wad.isLoged(key)) {
										 %>
										 <center> 文件正在处理中，请稍后...</center>
										 <div class="progress">
										  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
										  </div>
										</div>
										<%
											}
										%>
									<% }%>
								</td>
							</tr>
							
							<tr>
								<td style="word-break: break-all;">
									<%
										try {
											txt = wad.getText(key);
										} catch (Exception e) {
											txt = e.getMessage();
										}
										if (txt == null) {
											txt = "";//暂无法获取文本信息
										}
										if (txt.length() > 200) {
											txt = txt.substring(0, 180) + "...";
										}
									%> <%=txt%> <%
 	if (wad.isLoged(key)) {
 %> <a href="<%=wad.getlogURL(key)%>">日志文件<!-- <img alt="日志文件"
										src="img/log.png"> --></a>
 <a href="clearDir.jsp?key=<%=key%>" title="用于重新构建预览" >清空</a> <%
 	} else {
 %><%
 	}
 %>
								</td>
							</tr>
							<%
								if (paht != null && !paht.isEmpty()) {
							%>
							<tr>
								<td><%=paht%></td>
							</tr>
							<%
								}
							%>
						</table>
					</div>
					<div class="panel-heading">
						<%
						 	if (wad.isLoged(key)&&isError) {
						 %>
						<div style="text-align: left; color: #f15a24;">
							<%=wad.getLogText(key) %>
						</div>
						<%
							}
						%>
					</div>
				</div>
			</div>
			<div class="col-md-3"></div>
		</div>
	</div>
	<script src="js/jquery11.3.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script type="text/javascript">
	<!--
	<%
	if(isOnlyOne){
		%>
		window.location ="<%=curentUrl %>";
		<%
	}
	if(!isHaveFile&&!isError){
		%>
		 window.setTimeout(function(){
			 window.location.href="path.jsp?key=<%=key%>";
		 },3000); 
		<%
	}
	%>
	//-->
	</script>
</body>
</html>